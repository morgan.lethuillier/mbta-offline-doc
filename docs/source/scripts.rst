.. _scripts:


Setup your production environment
=================================

.. _install-scripts:


Install the production scripts
------------------------------

.. code-block:: console

   mkdir -P ~/virgo
   cd ~/virgo
   git clone git@git.ligo.org:morgan.lethuillier/mbta-offline-prod.git prod
   
You may need to load you ssh key before with

.. code-block:: console

   eval $(ssh-agent)
   ssh-add $HOME/.ssh/your_prefered_public_key

See this `doc <https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent>`_ for more guidance on how to generate your ssh key and add it to your git account. 

Three directories should have been created:

* ``${HOME}/virgo/prod/scripts`` : some very simple scripts usefull to manage the production

* ``${HOME}/virgo/prod/v5`` : main production scripts

* ``${HOME}/virgo/prod/v5/cfg.O4`` : mbta skeleton config files 


Add the following lines to your .bashrc to add the scripts to your $PATH:

.. code-block:: console

   # ------ functions to add or remove a directory to/from the $PATH and avoid duplication
   pathadd() {
      newdir=${1%/}
      if [ -d "$1" ] && ! echo $PATH | grep -E -q "(^|:)$newdir($|:)" ; then
         if [ "$2" = "after" ] ; then
            PATH="$PATH:$newdir"
         else
            PATH="$newdir:$PATH"
         fi
      fi
   }

   pathrm() {
      PATH="$(echo $PATH | sed -e "s;\(^\|:\)${1%/}\(:\|\$\);\1\2;g" -e 's;^:\|:$;;g' -e 's;::;:;g')"
   }

   # ------ add scripts directory to $PATH
   pathadd "."
   pathadd "${HOME}/virgo/prod/scripts" after
   export PATH




.. _setmbta:

Configure your environment
----------------------------

To configure your environment, you can use `setmbta.sh <https://git.ligo.org/morgan.lethuillier/mbta-offline-prod/-/blob/master/scripts/setmbta.sh>`_.

.. code-block:: console

    source setmbta.sh [LOCAL/CENTRAL]  [MBTA_VERSION]
    # example: source setmbta.sh CENTRAL v5r28
    # example: source setmbta.sh CENTRAL last

``CENTRAL`` is for a version installed centrally (in /pbs/throng/virgo/virgoApp/).

``LOCAL`` is for a version you have installed locally (in general in ~/virgo/App).

``MBTA_VERSION`` is the name of the mbta version. ``last`` corresponds to the more recent version.

The script will configure your environment, and store the paths of the different libraries in ``$MBTAROOT``,  ``$FDROOT``, ``$FRROOT``, ``$FRVROOT``, ``$FFTWROOT``... 

These paths are then added to your  ``$PATH``, ``$LD_LIBRARY_PATH``, ``$LIBRARY_PATH`` variables.

With bash autocompletion, you should now have access to most of the Ligo/Virgo executables. 
For instance try to type ``Mb`` + ``TAB`` and you will see all the Mbta executables. Or type ``lal`` + ``TAB`` to see all the lalsuite tools.

To restore your inital path variables, you can unset you environment with:

.. code-block:: console

    source unsetmbta.sh

This is done automatically when executing ``setmbta.sh`` a second time to definine a new different version of mbta.
