.. _overview:


Quick software overview
==========================



The sources of all the applications / libraries used by mbta are are available in git:

https://git.ligo.org/virgo/virgoapp

The main packages are:

* **mbta**: https://git.ligo.org/virgo/virgoapp/mbta
and his doc: https://git.ligo.org/virgo/virgoapp/mbta/-/blob/master/doc/MBTA.pdf

* **The frame distribution library (Fd)**: https://git.ligo.org/virgo/virgoapp/Fd
and his doc: https://git.ligo.org/virgo/virgoapp/Fd/-/blob/master/doc/Fd.pdf

*The main purpose of the Fd library is to provide data in the frame format from and to files, shared memory and network connection. Basic data conditioning tools like channel filtering, resampling are included in Fd as well as frame manipulation like channels selection, frame resizing, trend frame building and frame merging between different streams. The frames handled by the Fd package could contain any frame structures, from time series to events.*

* **The frame library (Fr)**: https://git.ligo.org/virgo/virgoapp/Fr
and his doc: https://git.ligo.org/virgo/virgoapp/Fr/-/blob/master/doc/FrDoc.pdf

The frame is the data structure used by the whole LVK collaboration to store interferometer data. Typical frame durations are 1s (for online pipelines) to O(100s) - O(1000s) (in offline applications). 

*A frame is a unit of information containing all the information necessary for the understanding of the interferometer behavior over a finite time interval which integrates several samplings. It contains thus not only the sampling performed during the integrated time interval, but also those performed at a frequency smaller than the frame frequency.*

A frame is composed of a header followed by an arbitrary number of additional structures (time series, reconstructed events, simulated events). The frame library defines the different formats (FrVect, FrEvent, FrSimEvent, FrProcData, ...) and provide methods to manipulate them.


Centrally installed virgo software
----------------------------------

mbta and its dependencies are already installed at CCIN2P3. The packages can be found in:

.. code-block:: console

      /pbs/throng/virgo/virgoApp


Packages installation and compilation is controlled by PackageManagement based on the Configuration Management Tool (CMT).

https://git.ligo.org/virgo/virgoapp/PackageManagement

To configure cmt (for obscure and historical reason, you have to force a wrong architecture):

.. code-block:: console

      export UNAME=amd64_sl7
      export CMTPATH=/pbs/throng/virgo/virgoApp
      source $CMTPATH/CMT/v1r24/mgr/setup.sh


You now have access to the ``cmt`` utility. For instance, to display the dependencies of a package:

.. code-block:: console

      cd /pbs/throng/virgo/virgoApp/mbta/v5r36/cmt
      cmt show uses

``cmt`` just looks in the ``requirements`` file to find the dependencies. For this version of mbta, the ``requirement`` file starts with:

.. code-block:: console

      package mbta

      use Fd v8r59

So mbta v5r36 depends on Fd v8r59. Then ``cmt`` looks at the Fd dependencies in

.. code-block:: console

     /pbs/throng/virgo/virgoApp/Fd/v8r59/cmt/requirements


And finds:

.. code-block:: console

    package Fd

    use Frv   v4r38p3
    use Cfg   v10r05p5

Then looks at Frv and Cfg ``requirements`` and so on.


Configure CMT environment
----------------------------

To configure your environment, before using a package, just source the ``setup.sh`` file in the package cmt directory.
First, if not already done, configure ``cmt``

.. code-block:: console

      export UNAME=amd64_sl7
      export CMTPATH=/pbs/throng/virgo/virgoApp
      source $CMTPATH/CMT/v1r24/mgr/setup.sh

then source the desired ``setup.sh`` script. For instance to use mbta version v5r36

.. code-block:: console

    source /pbs/throng/virgo/virgoApp/mbta/v5r36/cmt/setup.sh


.. tip::

   A more practical way is to use ``setmbta.sh`` as explained :ref:`here<setmbta>`


