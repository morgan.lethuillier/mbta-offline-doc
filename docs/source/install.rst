.. _install-mbta:


Install mbta at ccin2p3 (optional)
==================================



Local installation using Package Management
-------------------------------------------

Local install is only needed if you have to do some modifications in mbta or in some of its attached libraries. Otherwise, jsut use the central version already installed. 

Add more recent tools / compilators to your $PATH:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: console

    export PATH=/pbs/throng/virgo/VCS-9.0/bin/:$PATH

(you can add it to your .bashrc or it will be added later by ``setmbta.sh``)

Defaut ``cmake`` version in CCIN2P3 is 2.8.12.2 (/usr/bin/cmake).

Version of ``cmake`` in /pbs/throng/virgo/VCS-9.0/bin is 3.14.6.

Mimimum version required to build mbta packages is 3.12.0.

Install CMT:
^^^^^^^^^^^^

Download the Pm script: https://git.ligo.org/virgo/virgoapp/PackageManagement/-/jobs/artifacts/master/download?job=build

Execute the bootstrap command. It will create an arborescence in $HOME/virgo and install the CMT package in $HOME/virgo/App/CMT

.. code-block:: console

      python3 Pm.zip bootstrap

The script also proposes to add these lines to your .bashrc:

.. code-block:: console

      # ------ Virgo working environment setup
      
      export MAKEFLAGS=""
      export VIRGODATA=$HOME/virgo/Data
      export VIRGOLOG=$HOME/virgo/Log
      export CMTPATH=$HOME/virgo/App
      export PM_INSTALLATION_DIR=$CMTPATH
      source $CMTPATH/CMT/v1r24/mgr/setup.sh
      export UNAME=$CMTCONFIG
      export SVNROOT=https://virgorun@svn.ego-gw.it/svn/advsw/
      export GITROOT=git@git.ligo.org:virgo/virgoapp

      # ------

You can accept and remove them later if you chose to use ``setmbta.sh`` to configure your environment once mbta is installed.

Otherwise, you can juste execute them in your current interactive session.

Install PackageManagement:
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: console

      python3 Pm.zip install PackageManagement v11r6

Now Pm is installed in $HOME/virgo/App/PackageManagement/v11r6/scripts/Pm and you don't need Pm.zip anymore.

Install other packages:
^^^^^^^^^^^^^^^^^^^^^^^

You can now install whatever packages you want. 
But if you ask to install directly mbta with a lot of dependencies, it may fail, for another obscure reason, with a message asking for a password and then the error: *No standard version tags found for Cm in version control repository - Dependency on Cm not fulfilled*

Here is the full arborescence of mbta dependencies for version v5r36:

.. code-block:: console

      ~/virgo/App/PackageManagement/v11r6/scripts/Pm install mbta v5r36
      -> mbta v5r36 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
      ->   Fd v8r59 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
      ->     Frv v4r38p3 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
      ->       Fr v8r45 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
                 PackageBuild v1r7 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
      ->         Matlab v8r0p1 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
            FFTW v3r3p85 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
      ->     Cfg v10r05p5 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
      ->       Cm v10r03p7 (to be installed in /pbs/home/m/morgan/inst/virgo/App)
                 PackageBuild v1r6 (to be installed in /pbs/home/m/morgan/inst/virgo/App)

**FIXME** - I don't know if the problem comes from the dependency to 2 different versions of PackageBuild or from too many dependencies to be installed. (check with Benoit or Emmanuel Pacaud)

So let's install ``FFTW`` first:

.. code-block:: console

     ~/virgo/App/PackageManagement/v11r6/scripts/Pm install FFTW v3r3p85

Then mbta:

.. code-block:: console

     ~/virgo/App/PackageManagement/v11r6/scripts/Pm install mbta v5r36

You should be ready to run the mbta pipeline. You can try to run MbtaRT:

.. code-block:: console

     ~/virgo/App/mbta/v5r36/amd64_sl7/MbtaRT.exe ~/virgo/App/mbta/v5r36/test/makeASD.cfg

If everything is correctly installed, MbtaRT.exe will be run and you will get an error message for missing files like this one:

.. code-block:: console

      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgDataSetRFlag> logFlag 1 - stdoutFlag 1 - cmTrace 0
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgConfigLoad> /pbs/home/m/morgan/inst/virgo/App/mbta/v5r36/test/makeASD.cfg loaded from file '/pbs/home/m/morgan/inst/virgo/App/mbta/v5r36/test/makeASD.cfg'
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgConfigLoad> /pbs/home/m/morgan/inst/virgo/App/mbta/v5r36/test/makeASD.cfg Ok - size 1139 bytes
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgDataSetCmDomainName> to NONE
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgIdleCm> Server makeASD launched - No CM
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgDataSetStateFnt> Transition  Idle to End installed
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgDataSetRFlag> logFlag 1 - stdoutFlag 1 - cmTrace 0
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgMsgLogReportOpen> try to open log stream
      makeASD@2023-10-04-00h08m01-UTC>INFO...-Fd version /pbs/home/m/morgan/inst/virgo/App/Fd/v8r59/src (compiled: Oct  4 2023 01:56:25)
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgDataSetMemoryFlag>  to 0
      makeASD@2023-10-04-00h08m01-UTC>FATAL..-Unused word(s) in config: "/data/dev/cbc2/O3-offline/HLV_data/data_chunk32.ffl 1262973400
      /data/dev/cbc2/O3-offline/HLV_data/data_chunk32.ffl 1262973400
      /data/dev/cbc2/O3-offline/HLV_data/data_chunk32.ffl 1262973400
      MBTA_WINDOWS -4096  2.9
      MBTA_TEMPLATE 34.228691 10.738419 -0.749934 -0.440224 .1 .1"
      makeASD@2023-10-04-00h08m01-UTC>INFO...-CfgClose> Done









