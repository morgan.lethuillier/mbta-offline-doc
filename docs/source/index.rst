Welcome to mbta offline prod's documentation!
=============================================


Contents
--------

.. toctree::
   :maxdepth: 2

   impatient
   general
   .. auth
   overview
   scripts
   install
   data
   cat1
   .. slurm
   .. injections
   .. preprocessing
   .. filtering
   .. postfiltering
   checks
   .. macros

