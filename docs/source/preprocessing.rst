Preprocessing
=============

.. _prepro:

The preprocessing consists of a single slurm job. It reads the aggregated or analysis-ready frames (in general from cvmfs, and with a frame length depending of the ifo) and creates new 1s frames with h(t) timeseries reduced to 4096 Hz and applies gating (i.e. applying a filter to smoothly set h(t) to 0) when the range drops below a configurable fraction of the median range). The gating is also applied for gps times in bad data quality segments (see :ref:`here<cat1>` for more details). The common h(t) injections produced by the rates & pop group are also superposed to the data during this step. Either 2 injection trains spaced by 12s, or 4 injections trains spaced by 6s are used.

Use `createPrepro.sh <https://git.ligo.org/morgan.lethuillier/mbta-offline-prod/-/blob/master/v5/createPrepro.sh>`_
(already present in your ~/virgo/prod/scripts and in your \$PATH if you followed the :ref:`instructions<scripts>`
to configure your production environment) to prepare a processing run:

   Section not ready yet
   
   
.. code-block:: console

    ./createPrepro.sh [STD/SSM] [DAT/INJ]  [chunk number] [prepro version number] ([injection batch])

For instance:

.. code-block:: console

   ./createPrepro.sh STD DAT 07 03
   
will create the config files to run a preprocessing labelled v03 of the data of chunk 07 for the standard (=allsky) search in directory ``/sps/virgo/USERS/mbta/O4/preprocessing/chunk07/v03``

.. code-block:: console

   ./createPrepro.sh STD INJ 07 03 1-3
   
will create the config files to preprocess the injections of chunk 07 of the standard (=allsky) search in directory
``/sps/virgo/USERS/mbta/O4/preprocessing/chunk17/v03/batch-1-3``. Injection trains 1 and 3 spaced by 12s will be used.

``[injection batch]`` option is either ``0-2`` for trains 0 and 2 spaced by 12s, or ``1-3`` for trains 1 and 3 spaced by 12s or ``0123`` for trains 0, 1, 2 and 3 spaced by 6s.


Using cvmfs analysis ready frames
---------------------------------



Using local analysis ready frames
---------------------------------

As there are still stability problems with cvmfs access, a full copy of O4a h(t) data has been copied in 
``/sps/virgo/USERS/mbta/O4/...``
The analysis ready frames have been reduced to 4 kHz at CIT and tranfered to CCIN2P3.
Details of this pre-precessing are given in ``/sps/virgo/USERS/mbta/O4/.../README``.
If you use the option ``kk`` in ``createPrepro.sh``, then all you have to do is submitting the slurm job with:

.. code-block:: console

   sbatch prepro.cfg > prepro.id

``prepro.id`` will contain the slurm job number that will be used later for integrity checks of the job.


Script content
--------------


   



