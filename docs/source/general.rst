.. _general:
   
General documentation
=====================


MBTA software documentation
---------------------------

An MBTA documentation updated for its v5 version can be found `here <https://git.ligo.org/virgo/virgoapp/mbta/-/blob/master/doc/MBTA.pdf>`_.

The documentation for the `v4 version <https://git.ligo.org/virgo/virgoapp/mbta/-/blob/v4r34p4/doc/Mbta.pdf>`_ also contains some interesting details on the mbta processing.

FdIOServer is used for the preprocessing and the documentation of the Fd library can be consulted `here <https://git.ligo.org/virgo/virgoapp/Fd/-/blob/master/doc/Fd.pdf>`_.


MBTA publication
----------------

[1] T. Adams et al. “Low-latency analysis pipeline for compact binary coalescences
in the advanced gravitational wave detector era”. In: Class. Quant.
Grav. 33.17 (2016), p. 175012.
`DOI: 10.1088/0264-9381/33/17/175012 <https://iopscience.iop.org/article/10.1088/0264-9381/33/17/175012>`_.
`arXiv:1512.02864 [gr-qc] <https://arxiv.org/abs/1512.02864>`_.
`[reader friendly version] <https://ar5iv.labs.arxiv.org/html/1512.02864>`_.


[2] F. Aubin et al. “The MBTA pipeline for detecting compact binary coalescences
in the third LIGO–Virgo observing run”. In: Class. Quant. Grav. 38.9 (2021),
p. 095004.
`DOI: 10.1088/1361-6382/abe913 <https://iopscience.iop.org/article/10.1088/1361-6382/abe913>`_.
`arXiv: 2012.11512 [gr-qc] <https://arxiv.org/abs/2012.11512>`_.
`[reader friendly version] <https://ar5iv.labs.arxiv.org/html/2012.11512>`_.


[3] Nicolas Andres et al. “Assessing the compact-binary merger candidates reported
by the MBTA pipeline in the LIGO–Virgo O3 run: probability of astrophysical
origin, classification, and associated uncertainties”. In: Class. Quant.
Grav. 39.5 (2022), p. 055002.
`DOI: 10.1088/1361-6382/ac482a <https://iopscience.iop.org/article/10.1088/1361-6382/ac482a>`_.
`arXiv: 2110.10997 [gr-qc] <https://arxiv.org/abs/2110.10997>`_.
`[reader friendly version] <https://ar5iv.labs.arxiv.org/html/2110.10997>`_.


[4] Christopher Allene et al. “Relative calibration of the LIGO and Virgo detectors using astrophysical
events from their third observing run”. In: Class. Quant. Grav. 39 (2022), p. 195019.
`DOI: 10.1088/1361-6382/ac8c7b <https://iopscience.iop.org/article/10.1088/1361-6382/ac8c7b>`_.
`arXiv: 2204.00337 [gr-qc] <https://arxiv.org/abs/2204.00337>`_.
`[reader friendly version] <https://ar5iv.labs.arxiv.org/html/2204.00337>`_.


[5] Florian Aubin. “Recherche à faible latence d’ondes gravitationnelles émises
lors de coalescences de binaires compactes durant la troisième période d’observation
de Advanced Virgo et Advanced LIGO”. 2020CHAMA024. 
`PhD thesis. 2020 <https://theses.hal.science/tel-03336046>`_.


[6] Vincent Juste. “Search for compact binary coalescence using a single gravitational waves
detector with the MBTA data analysis pipeline”.
`PhD thesis. 2023 <https://theses.hal.science/tel-04427063v1>`_.


[7] Elisa Nitoglia. “Gravitational-wave data analysis for standard and non-standard sources of compact binary coalescences in the third LIGO-Virgo observing run”. PhD thesis. 2023.
`PhD thesis. 2023 <https://theses.hal.science/tel-04577839v1>`_.



MBTA meetings
-------------

List of `2024 meetings <https://wiki.ligo.org/CBC/Searches/MBTA/2024>`_.

List of `2023 meetings <https://wiki.ligo.org/CBC/Searches/MBTA/2023>`_.



MBTA reviews
------------
Materials for the LVK internal review and sign-off for the O4 online and offline analysis are available on the `O4ReviewMeetings <https://wiki.ligo.org/CBC/Searches/MBTA/O4ReviewMeetings>`_, 
`O4bReviewMeetings <https://wiki.ligo.org/CBC/Searches/MBTA/O4bReviewMeetings>`_,
and `O4aSSMReview <https://wiki.ligo.org/CBC/Searches/MBTA/O4aSSMReview>`_.



MBTA offline results
--------------------

Results of the `O4a offline allsky analysis <https://virgo.in2p3.fr/mbta/O4/std/CommonO4/index.html>`_ (detection of GW signals from coalescences of compact objects (black holes or neutron stars))

Results of the `O4a offline subsolar analysis <https://igwn.virgo.in2p3.fr/mbta/O4/ssm/CommonO4/index.html>`_ (detection of GW signals from coalescences of compact objects including at least one subsolar source)


