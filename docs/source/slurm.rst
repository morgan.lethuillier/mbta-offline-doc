slurm + cvmfs + scitokens = ?
=============================

.. warning::

   Section not ready yet
   

.. _slurm:

slurm
-----

Local farm at ccin2p3

https://gwdatafind.readthedocs.io/

https://computing.docs.ligo.org/guide/auth/scitokens/


cvmfs
-----

LVK data are distributed over cvmfs

.. code-block:: console

   source /cvmfs/software.igwn.org/conda/etc/profile.d/conda.sh
   conda activate igwn-py310 > /dev/null 2>&1
   htgettoken --vaultserver vault.ligo.org --issuer igwn --audience https://datafind.ligo.org --scope gwdatafind.read
   htdecodetoken -H
   python -m gwdatafind -r datafind.ligo.org -o H -t H1_HOFT_C00 --latest


scitokens
---------
authentification 
vault
bearer


accessing cvmfs from a slurm job
--------------------------------
myscripts

The solution is ``setsid()`` and is given in slide 12 of `this presentation <https://indico.ego-gw.it/event/42/contributions/853/attachments/592/1002/t2ucl-virgo-comp-centres-workshop-20191129.pdf>`_
(associated issue `#4392 <https://git.ligo.org/computing/helpdesk/-/issues/4392>`_)



